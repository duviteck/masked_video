package com.vice3.maskedvideo.dagger;

import com.vice3.maskedvideo.MaskedVideoApplication;
import com.vice3.maskedvideo.model.TextMaskModel;
import com.vice3.maskedvideo.model.VideoModel;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component (modules = {ApplicationModule.class})
public interface ApplicationComponent {
  MaskedVideoApplication getApplication();
  VideoModel getVideoModel();
  TextMaskModel getTextMaskModel();
}
