package com.vice3.maskedvideo.dagger;

import android.support.annotation.NonNull;

import com.vice3.maskedvideo.MaskedVideoApplication;
import com.vice3.maskedvideo.model.TextMaskModel;
import com.vice3.maskedvideo.model.VideoModel;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ApplicationModule {
  @NonNull
  private final MaskedVideoApplication mApplication;

  public ApplicationModule(@NonNull MaskedVideoApplication application) {
    mApplication = application;
  }

  @Singleton
  @Provides
  public MaskedVideoApplication provideApplication() {
    return mApplication;
  }

  @Singleton
  @Provides
  public VideoModel provideVideoModel() {
    return new VideoModel(mApplication);
  }

  @Singleton
  @Provides
  public TextMaskModel provideTextMaskModel() {
    return new TextMaskModel();
  }
}
