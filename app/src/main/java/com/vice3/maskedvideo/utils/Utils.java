package com.vice3.maskedvideo.utils;

import android.content.res.Resources;
import android.util.DisplayMetrics;

public class Utils {

  public static int dpToPx(float dp){
    DisplayMetrics metrics = Resources.getSystem().getDisplayMetrics();
    float px = dp * (metrics.densityDpi / 160f);
    return Math.round(px);
  }
}
