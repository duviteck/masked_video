package com.vice3.maskedvideo.mvp.presenter;

import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.UiThread;

import com.vice3.maskedvideo.mvp.view.MvpView;

import java.lang.ref.WeakReference;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

public class BasePresenter<V extends MvpView> implements Presenter<V> {

  @Nullable
  private WeakReference<V> mViewRef;

  @NonNull
  private CompositeSubscription mViewSubscription;

  @UiThread
  @Override
  @CallSuper
  public void attachView(@NonNull V view) {
    mViewRef = new WeakReference<>(view);
    mViewSubscription = new CompositeSubscription();
  }

  /**
   * Use this to register view interaction
   * You can be sure view will never be null in subscribe() method
   *
   * @see #attachView(MvpView)
   **/
  protected final void bind(@NonNull Subscription subscription) {
    mViewSubscription.add(subscription);
  }

  @UiThread
  @Nullable
  public final V getView() {
    return mViewRef == null ? null : mViewRef.get();
  }

  @UiThread
  @Override
  @CallSuper
  public void detachView() {
    mViewSubscription.unsubscribe();
    if (mViewRef != null) {
      mViewRef.clear();
      mViewRef = null;
    }
  }
}
