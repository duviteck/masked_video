package com.vice3.maskedvideo.mvp.view;

import com.vice3.maskedvideo.mvp.presenter.Presenter;

class ActivityMvpNonConfigurationInstances<V extends MvpView, P extends Presenter<V>> {
  final P presenter;
  final Object externalInstance;

  ActivityMvpNonConfigurationInstances(P presenter, Object externalInstance) {
    this.presenter = presenter;
    this.externalInstance = externalInstance;
  }
}
