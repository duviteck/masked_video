package com.vice3.maskedvideo.mvp.presenter;

import android.support.annotation.NonNull;
import android.support.annotation.UiThread;

import com.vice3.maskedvideo.mvp.view.MvpView;

public interface Presenter<V extends MvpView> {
  @UiThread
  void attachView(@NonNull V view);

  @UiThread
  void detachView();
}