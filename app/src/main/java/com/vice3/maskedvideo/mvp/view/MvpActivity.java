package com.vice3.maskedvideo.mvp.view;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.vice3.maskedvideo.MaskedVideoApplication;
import com.vice3.maskedvideo.mvp.presenter.Presenter;

import butterknife.ButterKnife;

public abstract class MvpActivity<V extends MvpView, P extends Presenter<V>>
    extends AppCompatActivity implements MvpView {
  private P mPresenter;

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    injectDependencies();
    super.onCreate(savedInstanceState);
    setContentView(getContentId());

    @SuppressWarnings("unchecked")
    final ActivityMvpNonConfigurationInstances<V, P> nci =
        (ActivityMvpNonConfigurationInstances<V, P>) getLastCustomNonConfigurationInstance();
    mPresenter = restorePresenter(nci);
    if (nci != null && nci.externalInstance != null) {
      restoreConfigurationInstances(nci.externalInstance);
    }
    ButterKnife.bind(this);
  }

  @Override
  protected void onStart() {
    super.onStart();
    mPresenter.attachView(getMvpView());
  }

  @Override
  protected void onStop() {
    super.onStop();
    mPresenter.detachView();
  }

  private P restorePresenter(@Nullable ActivityMvpNonConfigurationInstances<V, P> nci) {
    return nci != null && nci.presenter != null ? nci.presenter : createPresenter();
  }

  @NonNull
  public abstract P createPresenter();

  protected P getPresenter() {
    return mPresenter;
  }

  @Override
  public final Object onRetainCustomNonConfigurationInstance() {
    return new ActivityMvpNonConfigurationInstances<>(mPresenter, onRetainExternalConfigurationInstance());
  }

  protected void restoreConfigurationInstances(@NonNull Object nci) {
  }

  protected Object onRetainExternalConfigurationInstance() {
    return null;
  }

  @LayoutRes
  protected abstract int getContentId();

  @NonNull
  protected MaskedVideoApplication getApp() {
    return (MaskedVideoApplication) getApplicationContext();
  }

  @NonNull
  protected abstract V getMvpView();

  protected void injectDependencies() {
  }
}
