package com.vice3.maskedvideo.entity;

import android.support.annotation.ColorRes;
import android.support.annotation.NonNull;

public class TextMask {

  @NonNull
  private final String mText;

  private final @ColorRes int mMaskColor;

  private final @ColorRes int mMaskBackgroundColor;

  public TextMask(@NonNull String text, int maskColor, int backgroundColor) {
    mText = text;
    mMaskColor = maskColor;
    mMaskBackgroundColor = backgroundColor;
  }

  @NonNull
  public String getText() {
    return mText;
  }

  public @ColorRes int getMaskColor() {
    return mMaskColor;
  }

  public @ColorRes int getMaskBackgroundColor() {
    return mMaskBackgroundColor;
  }
}
