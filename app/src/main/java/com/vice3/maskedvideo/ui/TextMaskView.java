package com.vice3.maskedvideo.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.support.annotation.ColorInt;
import android.support.annotation.ColorRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import com.vice3.maskedvideo.R;

import static com.vice3.maskedvideo.utils.Utils.dpToPx;

public class TextMaskView extends View {

  private static final String DEFAULT_TEXT = "V";
  private static final @ColorInt int DEFAULT_TEXT_COLOR = Color.TRANSPARENT;
  private static final @ColorInt int DEFAULT_BACKGROUND_COLOR = Color.BLACK;
  private static final int DEFAULT_TEXT_SIZE = dpToPx(400f);

  @SuppressWarnings("NullableProblems")
  @NonNull
  private String mText;
  private int mTextDrawingLeft;
  private int mTextDrawingBottom;

  @NonNull
  private Paint mMaskPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

  @NonNull
  private Rect mMaskDrawingRect = new Rect();

  @NonNull
  private int[] mMaskBgColorARGB = new int[4];

  public TextMaskView(Context context) {
    this(context, null);
  }

  public TextMaskView(Context context, @Nullable AttributeSet attrs) {
    this(context, attrs, 0);
  }

  public TextMaskView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    init(context, attrs);
  }

  private void init(@NonNull Context context, @Nullable AttributeSet attrs) {
    // needed for correct PorterDuffXfermode work
    setLayerType(View.LAYER_TYPE_SOFTWARE, null);

    mMaskPaint.setTypeface(Typeface.DEFAULT_BOLD);
    mMaskPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC));

    // load attrs
    if (attrs != null) {
      TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.TextMaskView, 0, 0);
      try {
        mMaskPaint.setColor(ta.getColor(
            R.styleable.TextMaskView_textColor, DEFAULT_TEXT_COLOR));
        mMaskPaint.setTextSize(ta.getDimensionPixelSize(
            R.styleable.TextMaskView_textSize, DEFAULT_TEXT_SIZE));

        mMaskPaint.setColor(DEFAULT_TEXT_COLOR);
        mMaskPaint.setTextSize(DEFAULT_TEXT_SIZE);

        int backgroundColor = ta.getColor(
            R.styleable.TextMaskView_backgroundColor, DEFAULT_BACKGROUND_COLOR);
        setMaskBackgroundColorARGB(backgroundColor);

        String text = ta.getString(R.styleable.TextMaskView_text);
        setText(TextUtils.isEmpty(text) ? DEFAULT_TEXT : text);
      } finally {
        ta.recycle();
      }
    }
  }

  private void calcTextDrawingPos() {
    // we want to center mask in view
    int xOffset = (getWidth() - mMaskDrawingRect.width()) / 2;
    int yOffset = (getHeight() - mMaskDrawingRect.height()) / 2;

    mTextDrawingLeft = xOffset - mMaskDrawingRect.left;
    mTextDrawingBottom = yOffset + mMaskDrawingRect.height() - mMaskDrawingRect.bottom;
  }

  @Override
  protected void onSizeChanged(int w, int h, int oldw, int oldh) {
    super.onSizeChanged(w, h, oldw, oldh);
    calcTextDrawingPos();
  }

  @Override
  protected void onDraw(Canvas canvas) {
    super.onDraw(canvas);

    canvas.drawARGB(
        mMaskBgColorARGB[0],
        mMaskBgColorARGB[1],
        mMaskBgColorARGB[2],
        mMaskBgColorARGB[3]);

    canvas.drawText(mText, mTextDrawingLeft, mTextDrawingBottom, mMaskPaint);
  }

  @Override
  protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
    int width = measureSize(widthMeasureSpec, getDesiredWidth());
    int height = measureSize(heightMeasureSpec, getDesiredHeight());
    setMeasuredDimension(width, height);
  }

  private int measureSize(int measureSpec, int desiredSize) {
    int mode = MeasureSpec.getMode(measureSpec);
    int outerSize = MeasureSpec.getSize(measureSpec);

    if (mode == MeasureSpec.EXACTLY) {
      return outerSize;
    } else if (mode == MeasureSpec.AT_MOST) {
      return Math.min(desiredSize, outerSize);
    } else {
      return desiredSize;
    }
  }

  private int getDesiredWidth() {
    return mMaskDrawingRect.width() + getPaddingLeft() + getPaddingRight();
  }

  private int getDesiredHeight() {
    return mMaskDrawingRect.height() + getPaddingTop() + getPaddingBottom();
  }

  public void setText(@NonNull String text) {
    if (TextUtils.equals(mText, text)) {
      return;
    }

    mText = text;
    mMaskPaint.getTextBounds(mText, 0, mText.length(), mMaskDrawingRect);
    calcTextDrawingPos();

    invalidate();
  }

  public void setMaskColor(@ColorRes int maskColorRes) {
    int maskColor = getContext().getResources().getColor(maskColorRes);
    mMaskPaint.setColor(maskColor);
    invalidate();
  }

  public void setMaskBackgroundColor(@ColorRes int backgroundColorRes) {
    int bgColor = getContext().getResources().getColor(backgroundColorRes);
    setMaskBackgroundColorARGB(bgColor);
    invalidate();
  }

  private void setMaskBackgroundColorARGB(@ColorInt int bgColor) {
    mMaskBgColorARGB[0] = Color.alpha(bgColor);
    mMaskBgColorARGB[1] = Color.red(bgColor);
    mMaskBgColorARGB[2] = Color.green(bgColor);
    mMaskBgColorARGB[3] = Color.blue(bgColor);
  }
}

