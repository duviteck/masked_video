package com.vice3.maskedvideo.ui.home;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.vice3.maskedvideo.R;
import com.vice3.maskedvideo.entity.TextMask;
import com.vice3.maskedvideo.ui.TextMaskView;

import java.util.ArrayList;
import java.util.List;

class TextMasksPagerAdapter extends PagerAdapter {

  @NonNull
  private final LayoutInflater mInflater;

  @NonNull
  private final List<TextMask> mTextMasks;

  public TextMasksPagerAdapter(@NonNull Context context, @NonNull List<TextMask> textMasks) {
    mInflater = LayoutInflater.from(context);
    mTextMasks = new ArrayList<>(textMasks);
  }

  @Override
  public int getCount() {
    return mTextMasks.size();
  }

  @Override
  public boolean isViewFromObject(View view, Object object) {
    return view == object;
  }

  @Override
  public Object instantiateItem(ViewGroup container, int position) {
    TextMaskView maskView = (TextMaskView) mInflater.inflate(R.layout.item_mask, container, false);

    TextMask textMask = getTextMask(position);
    maskView.setText(textMask.getText());
    maskView.setMaskColor(textMask.getMaskColor());
    maskView.setMaskBackgroundColor(textMask.getMaskBackgroundColor());

    container.addView(maskView, 0);
    return maskView;
  }

  @NonNull
  private TextMask getTextMask(int position) {
    return mTextMasks.get(position);
  }

  @Override
  public void destroyItem(ViewGroup container, int position, Object object) {
    container.removeView((View) object);
  }
}
