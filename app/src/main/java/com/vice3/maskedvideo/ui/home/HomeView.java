package com.vice3.maskedvideo.ui.home;

import android.net.Uri;
import android.support.annotation.NonNull;

import com.vice3.maskedvideo.entity.TextMask;
import com.vice3.maskedvideo.mvp.view.MvpView;

import java.util.List;

public interface HomeView extends MvpView {
  void onTextMasksChanged(@NonNull List<TextMask> textMasks);
  void startBackgroundVideo(@NonNull Uri backgroundVideoUri, float volume);
}
