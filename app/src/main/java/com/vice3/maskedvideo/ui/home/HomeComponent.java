package com.vice3.maskedvideo.ui.home;

import com.vice3.maskedvideo.dagger.ApplicationComponent;
import com.vice3.maskedvideo.dagger.BaseScope;

import dagger.Component;

@BaseScope
@Component (dependencies = ApplicationComponent.class)
public interface HomeComponent {
  HomePresenter getPresenter();
}
