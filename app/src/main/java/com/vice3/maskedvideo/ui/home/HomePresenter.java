package com.vice3.maskedvideo.ui.home;

import android.support.annotation.NonNull;

import com.vice3.maskedvideo.model.TextMaskModel;
import com.vice3.maskedvideo.model.VideoModel;
import com.vice3.maskedvideo.mvp.presenter.BasePresenter;

import javax.inject.Inject;

public class HomePresenter extends BasePresenter<HomeView> {

  @NonNull
  private final VideoModel mVideoModel;

  @NonNull
  private final TextMaskModel mTextMaskModel;

  @Inject
  public HomePresenter(@NonNull VideoModel videoModel, @NonNull TextMaskModel textMaskModel) {
    mVideoModel = videoModel;
    mTextMaskModel = textMaskModel;
  }

  @Override
  public void attachView(@NonNull HomeView view) {
    super.attachView(view);

    view.onTextMasksChanged(mTextMaskModel.getMasksForDemo());

    view.startBackgroundVideo(mVideoModel.getBackgroundVideoUri(), mVideoModel.getVideoVolume());
  }
}
