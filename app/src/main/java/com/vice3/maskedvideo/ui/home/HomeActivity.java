package com.vice3.maskedvideo.ui.home;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.widget.VideoView;

import com.vice3.maskedvideo.R;
import com.vice3.maskedvideo.entity.TextMask;
import com.vice3.maskedvideo.mvp.view.MvpActivity;

import java.util.List;

import butterknife.BindView;
import me.relex.circleindicator.CircleIndicator;

public class HomeActivity extends MvpActivity<HomeView, HomePresenter> implements HomeView {

  @BindView(R.id.video_view) VideoView mVideoView;
  @BindView(R.id.masks_view_pager) ViewPager mMasksViewPager;
  @BindView(R.id.view_pager_indicator) CircleIndicator mViewPagerIndicator;

  @NonNull
  private HomeComponent mComponent;

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    mVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
      @Override
      public void onCompletion(MediaPlayer mp) {
        // We want to play video in a loop -> start it again on completion
        mVideoView.seekTo(0);
        mVideoView.start();
      }
    });
  }

  @Override
  protected void injectDependencies() {
    mComponent = DaggerHomeComponent.builder()
        .applicationComponent(getApp().getApplicationComponent())
        .build();
  }

  @NonNull
  @Override
  public HomePresenter createPresenter() {
    return mComponent.getPresenter();
  }

  @Override
  protected int getContentId() {
    return R.layout.activity_main;
  }

  @NonNull
  @Override
  protected HomeView getMvpView() {
    return this;
  }

  @Override
  public void onTextMasksChanged(@NonNull List<TextMask> textMasks) {
    PagerAdapter masksPagerAdapter = new TextMasksPagerAdapter(this, textMasks);
    mMasksViewPager.setAdapter(masksPagerAdapter);
    mViewPagerIndicator.setViewPager(mMasksViewPager);
  }

  @Override
  public void startBackgroundVideo(@NonNull Uri backgroundVideoUri, final float volume) {
    mVideoView.setVideoURI(backgroundVideoUri);
    mVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
      @Override
      public void onPrepared(MediaPlayer mp) {
        mp.setVolume(volume, volume);
      }
    });
    mVideoView.start();
  }
}
