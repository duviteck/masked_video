package com.vice3.maskedvideo.model;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.RawRes;

import com.vice3.maskedvideo.MaskedVideoApplication;
import com.vice3.maskedvideo.R;

import javax.inject.Inject;

public class VideoModel {

  private static final @RawRes int BACKGROUND_VIDEO_RES = R.raw.demo_sport;
  private static final float VOLUME_MUTE = 0f;
  private static final float VOLUME_FULL = 1f;

  private final MaskedVideoApplication mApplication;

  @Inject
  public VideoModel(@NonNull MaskedVideoApplication application) {
    mApplication = application;
  }

  public Uri getBackgroundVideoUri() {
    String path = "android.resource://" + mApplication.getPackageName() + "/" + BACKGROUND_VIDEO_RES;
    return Uri.parse(path);
  }

  public float getVideoVolume() {
    return VOLUME_MUTE;
  }
}
