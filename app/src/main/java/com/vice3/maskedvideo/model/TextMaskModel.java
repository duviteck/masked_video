package com.vice3.maskedvideo.model;

import android.support.annotation.NonNull;

import com.vice3.maskedvideo.R;
import com.vice3.maskedvideo.entity.TextMask;

import java.util.Arrays;
import java.util.List;

public class TextMaskModel {

  public TextMaskModel() {
  }

  @NonNull
  public List<TextMask> getMasksForDemo() {
    return Arrays.asList(
      new TextMask("1", R.color.mask_color_red, android.R.color.black),
      new TextMask("2", R.color.mask_color_red, android.R.color.black),
      new TextMask("3", R.color.mask_color_green, android.R.color.black),
      new TextMask("4", R.color.mask_color_green, android.R.color.black),
      new TextMask("5", R.color.mask_color_blue, android.R.color.black),
      new TextMask("6", R.color.mask_color_blue, android.R.color.black)
    );
  }
}
