package com.vice3.maskedvideo;

import android.app.Application;
import android.os.StrictMode;
import android.support.v7.app.AppCompatDelegate;

import com.vice3.maskedvideo.dagger.ApplicationComponent;
import com.vice3.maskedvideo.dagger.ApplicationModule;
import com.vice3.maskedvideo.dagger.DaggerApplicationComponent;

public class MaskedVideoApplication extends Application {
  private ApplicationComponent mApplicationComponent;

  @Override
  public void onCreate() {
    super.onCreate();

    AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    if (BuildConfig.DEBUG) {
      StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
          .detectAll()
          .penaltyLog()
          .build()
      );
      StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
          .detectAll()
          .detectActivityLeaks()
          .detectLeakedClosableObjects()
          .penaltyLog()
          .build()
      );
    }

    mApplicationComponent = createApplicationComponent();
  }

  protected ApplicationComponent createApplicationComponent() {
    return DaggerApplicationComponent.builder()
        .applicationModule(new ApplicationModule(this))
        .build();
  }

  public ApplicationComponent getApplicationComponent() {
    return mApplicationComponent;
  }


}
